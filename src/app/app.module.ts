import {
	ApplicationRef,
	DoBootstrap,
	NgModule,
} from '@angular/core';
import { TitleStrategy } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from 'modules/app-routing/app-routing.module';

import { AppComponent } from 'app/app.component';
import { TitlePagesResolver } from 'modules/shared/resolvers';
import { SharedModule } from 'modules/shared/shared.module';

@NgModule({
	imports: [
		BrowserModule,
		AppRoutingModule,
		SharedModule,
	],
	declarations: [AppComponent],
	providers: [
		{
			provide: TitleStrategy,
			useClass: TitlePagesResolver,
		},
	],
	bootstrap: [],
	exports: [],
})
export class AppModule implements DoBootstrap {

	public ngDoBootstrap(appRef: ApplicationRef): void {
		appRef.bootstrap(AppComponent, 'app-root');
	}
}
