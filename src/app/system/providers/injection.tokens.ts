import { InjectionToken } from "@angular/core";

const DefaultFallbackSrc = new InjectionToken<string>(
	'Default_fallback_value', {
		providedIn: 'root',
		factory: () => 'assets/images/oops.webp',
	},
);

export const InjectionTokens = {
	DefaultFallbackSrc,
};
