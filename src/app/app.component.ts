import {
	ChangeDetectionStrategy,
	Component,
} from "@angular/core";

import { AbstractComponent } from "app/system/abstract";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent extends AbstractComponent {

	constructor() {
		super({
			name: 'app-root',
		});
	}
}
