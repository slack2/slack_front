import {
	ChangeDetectionStrategy,
	Component,
} from "@angular/core";

import { AbstractComponent } from "app/system";

@Component({
	selector: 'app-auth-base',
	templateUrl: './auth-base.component.html',
	styleUrls: ['auth-base.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthBaseComponent extends AbstractComponent {
	constructor() {
		super({
			name: 'app-auth-base',
		});
	}
}
