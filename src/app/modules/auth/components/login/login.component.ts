import {
	ChangeDetectionStrategy,
	Component,
} from "@angular/core";

import { AbstractComponent } from "app/system";

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent extends AbstractComponent {
	constructor() {
		super({
			name: 'app-login',
		});
	}
}
