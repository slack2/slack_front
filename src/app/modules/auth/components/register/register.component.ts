import {
	ChangeDetectionStrategy,
	Component,
	OnInit,
} from '@angular/core';
import {
	FormGroup,
	FormControl,
	Validators,
} from '@angular/forms';

import { AbstractComponent } from 'app/system';

import * as Params from './register.params';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent extends AbstractComponent implements OnInit {
	public form!: FormGroup<Params.IRegisterFormGroup>;
	public nameControl!: FormControl<string>;
	public passwordControl!: FormControl<string>;

	constructor() {
		super({
			name: 'app-register',
		});
	}

	public ngOnInit(): void {
		this.nameControl = new FormControl('', {
			nonNullable: true,
			validators: [Validators.required],
		});

		this.passwordControl = new FormControl('', {
			nonNullable: true,
			validators: [
				Validators.required,
				Validators.minLength(5),
			],
		});

		this.form = new FormGroup({
			name: this.nameControl,
			password: this.passwordControl,
		});
	}
}
