import { FormControl } from "@angular/forms";

export interface IRegisterFormGroup {
	name: FormControl<string>,
	password: FormControl<string>,
}
