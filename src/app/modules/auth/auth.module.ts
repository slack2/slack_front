import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

import { SharedModule } from "modules/shared/shared.module";
import { authRoutes } from "modules/auth/auth-routes/auth-routes";
import { AuthService } from "modules/auth/services";
import {
	AuthBaseComponent,
	LoginComponent,
	RegisterComponent,
} from "modules/auth/components";

@NgModule({
	imports: [
		RouterModule.forChild(authRoutes),
		SharedModule,
	],
	declarations: [
		AuthBaseComponent,
		LoginComponent,
		RegisterComponent,
	],
	providers: [AuthService],
	exports: [],
})
export class AuthModule {}
