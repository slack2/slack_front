import { Routes } from "@angular/router";

import {
	AuthBaseComponent,
	LoginComponent,
	RegisterComponent,
} from "modules/auth/components";

export const authRoutes: Routes = [
	{
		path: '',
		title: 'Login',
		component: AuthBaseComponent,
		children: [
			{
				path: 'login',
				title: 'Login',
				component: LoginComponent,
			},
			{
				path: 'register',
				title: 'Registration',
				component: RegisterComponent,
			},
		],
	},
];
