import {
	Pipe,
	PipeTransform,
} from '@angular/core';
import { ValidationErrors } from '@angular/forms';

import { FieldErrors } from 'modules/shared/pipes/system';

@Pipe({
	name: 'fieldError',
	pure: true,
})
export class FieldErrorPipe implements PipeTransform {
	public transform(errors: ValidationErrors): string {
		const [error]: string[] = Object.keys(errors);

		if (error in FieldErrors) {
			const generator = FieldErrors[error];
			return generator({...errors[error]});
		}

		return '';
	}
}
