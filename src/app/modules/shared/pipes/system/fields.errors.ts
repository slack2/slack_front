function required() {
	return 'Данное поле является обязательным';
}

function minlength({requiredLength, actualLength}: {requiredLength: number, actualLength: number}) {
	return `Минимальная длина ${requiredLength}, актуальная ${actualLength}`;
}

export const FieldErrors: Record<string, Function> = {
	required,
	minlength,
};
