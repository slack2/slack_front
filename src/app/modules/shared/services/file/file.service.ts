import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { firstValueFrom } from "rxjs";

@Injectable({
	providedIn: 'root',
})
export class FileService {
	private _cachedFiles: Map<string, string> = new Map();

	constructor(
		protected readonly http: HttpClient,
	) {}

	public async getFile(src: string): Promise<string> {
		const file: string | undefined = this._cachedFiles.get(src);

		if (file) {
			return file;
		}

		return this.requestFile(src);
	}

	protected async requestFile(src: string): Promise<string> {
		try {
			const response: string = await firstValueFrom(this.http.get(
				src,
				{
					responseType: 'text',
					observe: 'body',
				},
			));

			this._cachedFiles.set(src, response);
			return response;
		} catch (error: unknown) {
			console.error('File service get', error);
			return '';
		}
	}
}
