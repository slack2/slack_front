import { Injectable } from "@angular/core";

import {
	filter,
	Observable,
	Subject,
} from "rxjs";

import { IEvent } from "./event.interfaces";

@Injectable({
	providedIn: 'root',
})
export class EventService {
	private _flow$: Subject<IEvent> = new Subject();

	public get flow$(): Observable<IEvent> {
		return this._flow$.asObservable();
	}

	public emit(event: IEvent): void {
		this._flow$.next(event);
	}

	public subscribe(data: IEvent): Observable<IEvent> {
		return this._flow$
			.pipe(
				filter(event => {
					if (Array.isArray(data)) {
						return !!data.find(item => item.name === event.name);
					}

					return event.name === data.name;
				}),
			);
	}
}
