import {
	ChangeDetectionStrategy,
	Component,
	Input,
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { AbstractComponent } from 'app/system';

@Component({
	selector: 'app-input',
	templateUrl: './input.component.html',
	styleUrls: ['./input.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputComponent extends AbstractComponent<'test'> {
	@Input() public control!: FormControl;
	@Input() public placeholder: string = '';
	@Input() public type: string = 'text';
	@Input() public id: string = '';

	constructor() {
		super({
			name: 'app-input',
		});
	}
}
