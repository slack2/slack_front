import {
	ChangeDetectionStrategy,
	ChangeDetectorRef,
	Component,
	HostBinding,
	Input,
	OnInit,
} from '@angular/core';
import {
	DomSanitizer,
	SafeHtml,
} from '@angular/platform-browser';

import { AbstractComponent } from 'app/system';
import { FileService } from 'modules/shared/services';

@Component({
	selector: 'app-icon',
	templateUrl: './icon.component.html',
	styleUrls: ['./icon.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent extends AbstractComponent implements OnInit {
	@Input() public src: string = '';

	@HostBinding('innerHTML') protected innerHtmlHost: SafeHtml = '';

	constructor(
		protected readonly fileService: FileService,
		protected readonly domSanitizer: DomSanitizer,
		protected readonly cdr: ChangeDetectorRef,
	) {
		super({
			name: 'app-icon',
		});
	}

	public ngOnInit(): void {
		this.getFile();
	}

	protected async getFile(): Promise<void> {
		const result: string = await this.fileService.getFile(this.src);
		this.innerHtmlHost = this.domSanitizer.bypassSecurityTrustHtml(result);
		this.cdr.markForCheck();
	}
}
