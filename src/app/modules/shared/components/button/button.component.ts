import {
	ChangeDetectionStrategy,
	Component,
} from '@angular/core';

import { AbstractComponent } from 'app/system';

@Component({
	selector: 'button [app-button]',
	templateUrl: './button.component.html',
	styleUrls: ['./button.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent extends AbstractComponent {
	constructor() {
		super({
			name: 'app-button',
		});
	}
}
