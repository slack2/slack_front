import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';

import {
	EventService,
	FileService,
} from "modules/shared/services";
import { FallbackDirective } from "modules/shared/directives";
import {
	ButtonComponent,
	IconComponent,
	InputComponent,
} from "modules/shared/components";
import { FieldErrorPipe } from "modules/shared/pipes";

@NgModule({
	imports: [
		ReactiveFormsModule,
		CommonModule,
		HttpClientModule,
	],
	declarations: [
		ButtonComponent,
		InputComponent,
		IconComponent,

		FallbackDirective,

		FieldErrorPipe,
	],
	providers: [
		EventService,
		FileService,
	],
	exports: [
		ReactiveFormsModule,
		CommonModule,

		ButtonComponent,
		InputComponent,
		IconComponent,

		FallbackDirective,

		FieldErrorPipe,
	],
})
export class SharedModule {}
