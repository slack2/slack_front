import { Injectable } from "@angular/core";
import { Title } from "@angular/platform-browser";
import {
	RouterStateSnapshot,
	TitleStrategy,
} from "@angular/router";

@Injectable()
export class TitlePagesResolver extends TitleStrategy {

	constructor(
		private readonly title: Title,
	) {
		super();
	}

	public override updateTitle(snapshot: RouterStateSnapshot): void {
		const currentPageTitle: string | undefined = this.buildTitle(snapshot);

		if (currentPageTitle) {
			this.title.setTitle(`SLack | ${currentPageTitle}`);
		} else {
			this.title.setTitle('Slack');
		}
	}
}
