import {
	Directive,
	ElementRef,
	Inject,
	Input,
	OnInit,
	Renderer2,
} from "@angular/core";
import {
	fromEvent,
	merge,
	Observable,
	Subject,
	takeUntil,
} from "rxjs";

import {
	Destroy,
	InjectionTokens,
} from "app/system";

@Directive({
	selector: '[fallback]',
})
export class FallbackDirective extends Destroy implements OnInit {
	@Input() public fallback: string = '';

	private _unsubscribe$: Subject<void> = new Subject();

	constructor(
		@Inject(InjectionTokens.DefaultFallbackSrc) protected readonly defaultFallbackSrc: string,
		protected readonly elementRef: ElementRef<HTMLElement>,
		protected readonly renderer2: Renderer2,
	) {
		super();
	}

	public ngOnInit(): void {
		const {nativeElement}: ElementRef<HTMLElement> = this.elementRef;

		fromEvent(nativeElement, 'error')
			.pipe(takeUntil(this.takeUntil))
			.subscribe(() => {
				this._unsubscribe$.next();

				if (nativeElement instanceof HTMLImageElement) {
					this.renderer2.setProperty(nativeElement, 'src', this.fallback ? this.fallback : this.defaultFallbackSrc);
				}
			});

		fromEvent(nativeElement, 'load')
			.pipe(takeUntil(this.takeUntil))
			.subscribe(() => this._unsubscribe$.next());
	}

	protected get takeUntil(): Observable<void> {
		return merge(
			this.destroy$,
			this._unsubscribe$,
		);
	}
}
